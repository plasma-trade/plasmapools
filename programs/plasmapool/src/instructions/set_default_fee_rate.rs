use anchor_lang::prelude::*;

use crate::state::{FeeTier, PlasmaPoolsConfig};

#[derive(Accounts)]
pub struct SetDefaultFeeRate<'info> {
    pub plasmapools_config: Account<'info, PlasmaPoolsConfig>,

    #[account(mut, has_one = plasmapools_config)]
    pub fee_tier: Account<'info, FeeTier>,

    #[account(address = plasmapools_config.fee_authority)]
    pub fee_authority: Signer<'info>,
}

/*
   Updates the default fee rate on a FeeTier object.
*/
pub fn handler(ctx: Context<SetDefaultFeeRate>, default_fee_rate: u16) -> Result<()> {
    Ok(ctx
        .accounts
        .fee_tier
        .update_default_fee_rate(default_fee_rate)?)
}
