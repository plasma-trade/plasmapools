use anchor_lang::prelude::*;

use crate::state::{PlasmaPool, PlasmaPoolsConfig};

#[derive(Accounts)]
pub struct SetFeeRate<'info> {
    pub plasmapools_config: Account<'info, PlasmaPoolsConfig>,

    #[account(mut, has_one = plasmapools_config)]
    pub plasmapool: Account<'info, PlasmaPool>,

    #[account(address = plasmapools_config.fee_authority)]
    pub fee_authority: Signer<'info>,
}

pub fn handler(ctx: Context<SetFeeRate>, fee_rate: u16) -> Result<()> {
    Ok(ctx.accounts.plasmapool.update_fee_rate(fee_rate)?)
}
