use anchor_lang::prelude::*;

use crate::state::PlasmaPoolsConfig;

#[derive(Accounts)]
pub struct SetDefaultProtocolFeeRate<'info> {
    #[account(mut)]
    pub plasmapools_config: Account<'info, PlasmaPoolsConfig>,

    #[account(address = plasmapools_config.fee_authority)]
    pub fee_authority: Signer<'info>,
}

pub fn handler(
    ctx: Context<SetDefaultProtocolFeeRate>,
    default_protocol_fee_rate: u16,
) -> Result<()> {
    Ok(ctx
        .accounts
        .plasmapools_config
        .update_default_protocol_fee_rate(default_protocol_fee_rate)?)
}
