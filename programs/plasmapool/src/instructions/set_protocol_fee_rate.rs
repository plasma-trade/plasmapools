use anchor_lang::prelude::*;

use crate::state::{PlasmaPool, PlasmaPoolsConfig};

#[derive(Accounts)]
pub struct SetProtocolFeeRate<'info> {
    pub plasmapools_config: Account<'info, PlasmaPoolsConfig>,

    #[account(mut, has_one = plasmapools_config)]
    pub plasmapool: Account<'info, PlasmaPool>,

    #[account(address = plasmapools_config.fee_authority)]
    pub fee_authority: Signer<'info>,
}

pub fn handler(ctx: Context<SetProtocolFeeRate>, protocol_fee_rate: u16) -> Result<()> {
    Ok(ctx
        .accounts
        .plasmapool
        .update_protocol_fee_rate(protocol_fee_rate)?)
}
