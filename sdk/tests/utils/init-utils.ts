import * as anchor from "@project-serum/anchor";
import {
  InitTickArrayParams,
  OpenPositionParams,
  InitPoolParams,
  InitializeRewardParams,
  TICK_ARRAY_SIZE,
  PlasmaPoolContext,
  AccountFetcher,
  InitConfigParams,
  TickUtil,
  PriceMath,
  PlasmaPoolIx,
  PDAUtil,
  toTx,
} from "../../src";
import {
  generateDefaultConfigParams,
  generateDefaultInitFeeTierParams,
  generateDefaultInitPoolParams,
  generateDefaultInitTickArrayParams,
  generateDefaultOpenPositionParams,
} from "./test-builders";
import { PublicKey, Keypair } from "@solana/web3.js";
import {
  createAndMintToTokenAccount,
  createMint,
  mintToByAuthority,
  TickSpacing,
  ZERO_BN,
} from ".";
import { u64 } from "@solana/spl-token";
import { PoolUtil } from "../../src/utils/public/pool-utils";
import { MathUtil, PDA } from "@plasmatrade-so/common-sdk";

const defaultInitSqrtPrice = MathUtil.toX64_BN(new u64(5));

/**
 * Initialize a brand new PlasmaPoolsConfig account and construct a set of InitPoolParams
 * that can be used to initialize a pool with.
 * @param client - an instance of plasmapool client containing the program & provider
 * @param initSqrtPrice - the initial sqrt-price for this newly generated pool
 * @returns An object containing the params used to init the config account & the param that can be used to init the pool account.
 */
export async function buildTestPoolParams(
  ctx: PlasmaPoolContext,
  tickSpacing: number,
  defaultFeeRate = 3000,
  initSqrtPrice = defaultInitSqrtPrice,
  funder?: PublicKey
) {
  const { configInitInfo, configKeypairs } = generateDefaultConfigParams(ctx);
  await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();

  const { params: feeTierParams } = await initFeeTier(
    ctx,
    configInitInfo,
    configKeypairs.feeAuthorityKeypair,
    tickSpacing,
    defaultFeeRate
  );
  const poolInitInfo = await generateDefaultInitPoolParams(
    ctx,
    configInitInfo.plasmapoolsConfigKeypair.publicKey,
    feeTierParams.feeTierPda.publicKey,
    tickSpacing,
    initSqrtPrice,
    funder
  );
  return {
    configInitInfo,
    configKeypairs,
    poolInitInfo,
    feeTierParams,
  };
}

/**
 * Initialize a brand new set of PlasmaPoolsConfig & PlasmaPool account
 * @param client - an instance of plasmapool client containing the program & provider
 * @param initSqrtPrice - the initial sqrt-price for this newly generated pool
 * @returns An object containing the params used to initialize both accounts.
 */
export async function initTestPool(
  ctx: PlasmaPoolContext,
  tickSpacing: number,
  initSqrtPrice = defaultInitSqrtPrice,
  funder?: Keypair
) {
  const { configInitInfo, poolInitInfo, configKeypairs, feeTierParams } = await buildTestPoolParams(
    ctx,
    tickSpacing,
    3000,
    initSqrtPrice,
    funder?.publicKey
  );

  const tx = toTx(ctx, PlasmaPoolIx.initializePoolIx(ctx.program, poolInitInfo));
  if (funder) {
    tx.addSigner(funder);
  }

  return {
    txId: await tx.buildAndExecute(),
    configInitInfo,
    configKeypairs,
    poolInitInfo,
    feeTierParams,
  };
}

export async function initFeeTier(
  ctx: PlasmaPoolContext,
  configInitInfo: InitConfigParams,
  feeAuthorityKeypair: Keypair,
  tickSpacing: number,
  defaultFeeRate: number,
  funder?: Keypair
) {
  const params = generateDefaultInitFeeTierParams(
    ctx,
    configInitInfo.plasmapoolsConfigKeypair.publicKey,
    configInitInfo.feeAuthority,
    tickSpacing,
    defaultFeeRate,
    funder?.publicKey
  );

  const tx = toTx(ctx, PlasmaPoolIx.initializeFeeTierIx(ctx.program, params)).addSigner(
    feeAuthorityKeypair
  );
  if (funder) {
    tx.addSigner(funder);
  }

  return {
    txId: await tx.buildAndExecute(),
    params,
  };
}

export async function initializeReward(
  ctx: PlasmaPoolContext,
  rewardAuthorityKeypair: anchor.web3.Keypair,
  plasmapool: PublicKey,
  rewardIndex: number,
  funder?: Keypair
): Promise<{ txId: string; params: InitializeRewardParams }> {
  const provider = ctx.provider;
  const rewardMint = await createMint(provider);
  const rewardVaultKeypair = anchor.web3.Keypair.generate();

  const params = {
    rewardAuthority: rewardAuthorityKeypair.publicKey,
    funder: funder?.publicKey || ctx.wallet.publicKey,
    plasmapool,
    rewardMint,
    rewardVaultKeypair,
    rewardIndex,
  };

  const tx = toTx(ctx, PlasmaPoolIx.initializeRewardIx(ctx.program, params)).addSigner(
    rewardAuthorityKeypair
  );
  if (funder) {
    tx.addSigner(funder);
  }

  return {
    txId: await tx.buildAndExecute(),
    params,
  };
}

export async function initRewardAndSetEmissions(
  ctx: PlasmaPoolContext,
  rewardAuthorityKeypair: anchor.web3.Keypair,
  plasmapool: PublicKey,
  rewardIndex: number,
  vaultAmount: u64 | number,
  emissionsPerSecondX64: anchor.BN,
  funder?: Keypair
) {
  const {
    params: { rewardMint, rewardVaultKeypair },
  } = await initializeReward(ctx, rewardAuthorityKeypair, plasmapool, rewardIndex, funder);
  await mintToByAuthority(ctx.provider, rewardMint, rewardVaultKeypair.publicKey, vaultAmount);
  await toTx(
    ctx,
    PlasmaPoolIx.setRewardEmissionsIx(ctx.program, {
      rewardAuthority: rewardAuthorityKeypair.publicKey,
      plasmapool,
      rewardIndex,
      rewardVaultKey: rewardVaultKeypair.publicKey,
      emissionsPerSecondX64,
    })
  )
    .addSigner(rewardAuthorityKeypair)
    .buildAndExecute();
  return { rewardMint, rewardVaultKeypair };
}

export async function openPosition(
  ctx: PlasmaPoolContext,
  plasmapool: PublicKey,
  tickLowerIndex: number,
  tickUpperIndex: number,
  owner: PublicKey = ctx.provider.wallet.publicKey,
  funder?: Keypair
) {
  return openPositionWithOptMetadata(
    ctx,
    plasmapool,
    tickLowerIndex,
    tickUpperIndex,
    false,
    owner,
    funder
  );
}

export async function openPositionWithMetadata(
  ctx: PlasmaPoolContext,
  plasmapool: PublicKey,
  tickLowerIndex: number,
  tickUpperIndex: number,
  owner: PublicKey = ctx.provider.wallet.publicKey,
  funder?: Keypair
) {
  return openPositionWithOptMetadata(
    ctx,
    plasmapool,
    tickLowerIndex,
    tickUpperIndex,
    true,
    owner,
    funder
  );
}

async function openPositionWithOptMetadata(
  ctx: PlasmaPoolContext,
  plasmapool: PublicKey,
  tickLowerIndex: number,
  tickUpperIndex: number,
  withMetadata: boolean = false,
  owner: PublicKey = ctx.provider.wallet.publicKey,
  funder?: Keypair
) {
  const { params, mint } = await generateDefaultOpenPositionParams(
    ctx,
    plasmapool,
    tickLowerIndex,
    tickUpperIndex,
    owner,
    funder?.publicKey || ctx.provider.wallet.publicKey
  );
  let tx = withMetadata
    ? toTx(ctx, PlasmaPoolIx.openPositionWithMetadataIx(ctx.program, params))
    : toTx(ctx, PlasmaPoolIx.openPositionIx(ctx.program, params));
  tx.addSigner(mint);
  if (funder) {
    tx.addSigner(funder);
  }
  const txId = await tx.buildAndExecute();
  return { txId, params, mint };
}

export async function initTickArray(
  ctx: PlasmaPoolContext,
  plasmapool: PublicKey,
  startTickIndex: number,
  funder?: Keypair
): Promise<{ txId: string; params: InitTickArrayParams }> {
  const params = generateDefaultInitTickArrayParams(
    ctx,
    plasmapool,
    startTickIndex,
    funder?.publicKey
  );
  const tx = toTx(ctx, PlasmaPoolIx.initTickArrayIx(ctx.program, params));
  if (funder) {
    tx.addSigner(funder);
  }
  return { txId: await tx.buildAndExecute(), params };
}

export async function initTestPoolWithTokens(
  ctx: PlasmaPoolContext,
  tickSpacing: number,
  initSqrtPrice = defaultInitSqrtPrice
) {
  const provider = ctx.provider;

  const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
    ctx,
    tickSpacing,
    initSqrtPrice
  );

  const { tokenMintA, tokenMintB, plasmapoolPda } = poolInitInfo;
  const tokenAccountA = await createAndMintToTokenAccount(provider, tokenMintA, 15_000_000);
  const tokenAccountB = await createAndMintToTokenAccount(provider, tokenMintB, 15_000_000);
  return {
    poolInitInfo,
    configInitInfo,
    configKeypairs,
    plasmapoolPda,
    tokenAccountA,
    tokenAccountB,
  };
}

export async function initTickArrayRange(
  ctx: PlasmaPoolContext,
  plasmapool: PublicKey,
  startTickIndex: number,
  arrayCount: number,
  tickSpacing: number,
  aToB: boolean
): Promise<PDA[]> {
  const ticksInArray = tickSpacing * TICK_ARRAY_SIZE;
  const direction = aToB ? -1 : 1;
  const result: PDA[] = [];

  for (let i = 0; i < arrayCount; i++) {
    const { params } = await initTickArray(
      ctx,
      plasmapool,
      startTickIndex + direction * ticksInArray * i
    );
    result.push(params.tickArrayPda);
  }

  return result;
}

export type FundedPositionParams = {
  tickLowerIndex: number;
  tickUpperIndex: number;
  liquidityAmount: anchor.BN;
};

export async function withdrawPositions(
  ctx: PlasmaPoolContext,
  positionInfos: FundedPositionInfo[],
  tokenOwnerAccountA: PublicKey,
  tokenOwnerAccountB: PublicKey
) {
  const fetcher = new AccountFetcher(ctx.connection);
  await Promise.all(
    positionInfos.map(async (info) => {
      const pool = await fetcher.getPool(info.initParams.plasmapool);
      const position = await fetcher.getPosition(info.initParams.positionPda.publicKey);

      if (!pool) {
        throw new Error(`Failed to fetch pool - ${info.initParams.plasmapool}`);
      }

      if (!position) {
        throw new Error(`Failed to fetch position - ${info.initParams.plasmapool}`);
      }

      const priceLower = PriceMath.tickIndexToSqrtPriceX64(position.tickLowerIndex);
      const priceUpper = PriceMath.tickIndexToSqrtPriceX64(position.tickUpperIndex);

      const { tokenA, tokenB } = PoolUtil.getTokenAmountsFromLiquidity(
        position.liquidity,
        pool.sqrtPrice,
        priceLower,
        priceUpper,
        false
      );

      const numTicksInTickArray = pool.tickSpacing * TICK_ARRAY_SIZE;
      const lowerStartTick =
        position.tickLowerIndex - (position.tickLowerIndex % numTicksInTickArray);
      const tickArrayLower = PDAUtil.getTickArray(
        ctx.program.programId,
        info.initParams.plasmapool,
        lowerStartTick
      );
      const upperStartTick =
        position.tickUpperIndex - (position.tickUpperIndex % numTicksInTickArray);
      const tickArrayUpper = PDAUtil.getTickArray(
        ctx.program.programId,
        info.initParams.plasmapool,
        upperStartTick
      );

      await toTx(
        ctx,
        PlasmaPoolIx.decreaseLiquidityIx(ctx.program, {
          liquidityAmount: position.liquidity,
          tokenMinA: tokenA,
          tokenMinB: tokenB,
          plasmapool: info.initParams.plasmapool,
          positionAuthority: ctx.provider.wallet.publicKey,
          position: info.initParams.positionPda.publicKey,
          positionTokenAccount: info.initParams.positionTokenAccount,
          tokenOwnerAccountA,
          tokenOwnerAccountB,
          tokenVaultA: pool.tokenVaultA,
          tokenVaultB: pool.tokenVaultB,
          tickArrayLower: tickArrayLower.publicKey,
          tickArrayUpper: tickArrayUpper.publicKey,
        })
      ).buildAndExecute();

      await toTx(
        ctx,
        PlasmaPoolIx.collectFeesIx(ctx.program, {
          plasmapool: info.initParams.plasmapool,
          positionAuthority: ctx.provider.wallet.publicKey,
          position: info.initParams.positionPda.publicKey,
          positionTokenAccount: info.initParams.positionTokenAccount,
          tokenOwnerAccountA,
          tokenOwnerAccountB,
          tokenVaultA: pool.tokenVaultA,
          tokenVaultB: pool.tokenVaultB,
        })
      ).buildAndExecute();
    })
  );
}

export interface FundedPositionInfo {
  initParams: OpenPositionParams;
  publicKey: PublicKey;
  tokenAccount: PublicKey;
  mintKeypair: Keypair;
  tickArrayLower: PublicKey;
  tickArrayUpper: PublicKey;
}

export async function fundPositions(
  ctx: PlasmaPoolContext,
  poolInitInfo: InitPoolParams,
  tokenAccountA: PublicKey,
  tokenAccountB: PublicKey,
  fundParams: FundedPositionParams[]
): Promise<FundedPositionInfo[]> {
  const {
    plasmapoolPda: { publicKey: plasmapool },
    tickSpacing,
    tokenVaultAKeypair,
    tokenVaultBKeypair,
    initSqrtPrice,
  } = poolInitInfo;

  return await Promise.all(
    fundParams.map(async (param): Promise<FundedPositionInfo> => {
      const { params: positionInfo, mint } = await openPosition(
        ctx,
        plasmapool,
        param.tickLowerIndex,
        param.tickUpperIndex
      );

      const tickArrayLower = PDAUtil.getTickArray(
        ctx.program.programId,
        plasmapool,
        TickUtil.getStartTickIndex(param.tickLowerIndex, tickSpacing)
      ).publicKey;

      const tickArrayUpper = PDAUtil.getTickArray(
        ctx.program.programId,
        plasmapool,
        TickUtil.getStartTickIndex(param.tickUpperIndex, tickSpacing)
      ).publicKey;

      if (param.liquidityAmount.gt(ZERO_BN)) {
        const { tokenA, tokenB } = PoolUtil.getTokenAmountsFromLiquidity(
          param.liquidityAmount,
          initSqrtPrice,
          PriceMath.tickIndexToSqrtPriceX64(param.tickLowerIndex),
          PriceMath.tickIndexToSqrtPriceX64(param.tickUpperIndex),
          true
        );
        await toTx(
          ctx,
          PlasmaPoolIx.increaseLiquidityIx(ctx.program, {
            liquidityAmount: param.liquidityAmount,
            tokenMaxA: tokenA,
            tokenMaxB: tokenB,
            plasmapool: plasmapool,
            positionAuthority: ctx.provider.wallet.publicKey,
            position: positionInfo.positionPda.publicKey,
            positionTokenAccount: positionInfo.positionTokenAccount,
            tokenOwnerAccountA: tokenAccountA,
            tokenOwnerAccountB: tokenAccountB,
            tokenVaultA: tokenVaultAKeypair.publicKey,
            tokenVaultB: tokenVaultBKeypair.publicKey,
            tickArrayLower,
            tickArrayUpper,
          })
        ).buildAndExecute();
      }
      return {
        initParams: positionInfo,
        publicKey: positionInfo.positionPda.publicKey,
        tokenAccount: positionInfo.positionTokenAccount,
        mintKeypair: mint,
        tickArrayLower,
        tickArrayUpper,
      };
    })
  );
}

export async function initTestPoolWithLiquidity(ctx: PlasmaPoolContext) {
  const {
    poolInitInfo,
    configInitInfo,
    configKeypairs,
    plasmapoolPda,
    tokenAccountA,
    tokenAccountB,
  } = await initTestPoolWithTokens(ctx, TickSpacing.Standard);

  const tickArrays = await initTickArrayRange(
    ctx,
    plasmapoolPda.publicKey,
    22528, // to 33792
    3,
    TickSpacing.Standard,
    false
  );

  const fundParams: FundedPositionParams[] = [
    {
      liquidityAmount: new u64(100_000),
      tickLowerIndex: 27904,
      tickUpperIndex: 33408,
    },
  ];

  const positionInfos = await fundPositions(
    ctx,
    poolInitInfo,
    tokenAccountA,
    tokenAccountB,
    fundParams
  );

  return {
    poolInitInfo,
    configInitInfo,
    configKeypairs,
    positionInfo: positionInfos[0].initParams,
    tokenAccountA,
    tokenAccountB,
    tickArrays,
  };
}
