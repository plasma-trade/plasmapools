import * as anchor from "@project-serum/anchor";
import * as assert from "assert";
import {
  PlasmaPoolContext,
  AccountFetcher,
  TICK_ARRAY_SIZE,
  InitTickArrayParams,
  InitPoolParams,
  TickArrayData,
  PlasmaPoolIx,
  toTx,
} from "../../src";
import { TickSpacing, systemTransferTx, ONE_SOL } from "../utils";
import { initTestPool, initTickArray } from "../utils/init-utils";
import { generateDefaultInitTickArrayParams } from "../utils/test-builders";

describe("initialize_tick_array", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully init a TickArray account", async () => {
    const tickSpacing = TickSpacing.Standard;
    const { poolInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey);
    const startTick = TICK_ARRAY_SIZE * tickSpacing * 2;

    const tickArrayInitInfo = generateDefaultInitTickArrayParams(
      ctx,
      poolInitInfo.plasmapoolPda.publicKey,
      startTick
    );

    await toTx(ctx, PlasmaPoolIx.initTickArrayIx(ctx.program, tickArrayInitInfo)).buildAndExecute();
    assertTickArrayInitialized(ctx, tickArrayInitInfo, poolInitInfo, startTick);
  });

  it("successfully init a TickArray account with a negative index", async () => {
    const tickSpacing = TickSpacing.Standard;
    const { poolInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey);
    const startTick = TICK_ARRAY_SIZE * tickSpacing * -2;

    const tickArrayInitInfo = generateDefaultInitTickArrayParams(
      ctx,
      poolInitInfo.plasmapoolPda.publicKey,
      startTick
    );

    await toTx(ctx, PlasmaPoolIx.initTickArrayIx(ctx.program, tickArrayInitInfo)).buildAndExecute();
    assertTickArrayInitialized(ctx, tickArrayInitInfo, poolInitInfo, startTick);
  });

  it("succeeds when funder is different than account paying for transaction fee", async () => {
    const tickSpacing = TickSpacing.Standard;
    const { poolInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const funderKeypair = anchor.web3.Keypair.generate();
    await systemTransferTx(provider, funderKeypair.publicKey, ONE_SOL).buildAndExecute();
    await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey);
    const startTick = TICK_ARRAY_SIZE * tickSpacing * 3;
    await initTickArray(ctx, poolInitInfo.plasmapoolPda.publicKey, startTick, funderKeypair);
  });

  it("fails when start tick index is not a valid start index", async () => {
    const tickSpacing = TickSpacing.Standard;
    const { poolInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey);
    const startTick = TICK_ARRAY_SIZE * tickSpacing * 2 + 1;

    const params = generateDefaultInitTickArrayParams(
      ctx,
      poolInitInfo.plasmapoolPda.publicKey,
      startTick
    );

    try {
      await toTx(ctx, PlasmaPoolIx.initTickArrayIx(ctx.program, params)).buildAndExecute();
      assert.fail(
        "should fail if start-tick is not a multiple of tick spacing and num ticks in array"
      );
    } catch (e) {
      const error = e as Error;
      assert.match(error.message, /0x1771/); // InvalidStartTick
    }
  });

  async function assertTickArrayInitialized(
    ctx: PlasmaPoolContext,
    tickArrayInitInfo: InitTickArrayParams,
    poolInitInfo: InitPoolParams,
    startTick: number
  ) {
    let tickArrayData = (await fetcher.getTickArray(
      tickArrayInitInfo.tickArrayPda.publicKey
    )) as TickArrayData;
    assert.ok(tickArrayData.plasmapool.equals(poolInitInfo.plasmapoolPda.publicKey));
    assert.ok(tickArrayData.startTickIndex == startTick);
  }
});
