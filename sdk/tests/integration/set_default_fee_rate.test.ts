import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import {
  PlasmaPoolContext,
  AccountFetcher,
  PlasmaPoolData,
  InitPoolParams,
  PlasmaPoolIx,
  PDAUtil,
  toTx,
} from "../../src";
import { TickSpacing } from "../utils";
import { initTestPool } from "../utils/init-utils";
import { createInOrderMints, generateDefaultConfigParams } from "../utils/test-builders";

describe("set_default_fee_rate", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully set_default_fee_rate", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs, feeTierParams } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newDefaultFeeRate = 45;

    // Fetch initial plasmapool and check it is default
    let plasmapool_0 = (await fetcher.getPool(plasmapoolKey)) as PlasmaPoolData;
    assert.equal(plasmapool_0.feeRate, feeTierParams.defaultFeeRate);

    await toTx(
      ctx,
      PlasmaPoolIx.setDefaultFeeRateIx(ctx.program, {
        plasmapoolsConfig: plasmapoolsConfigKey,
        feeAuthority: feeAuthorityKeypair.publicKey,
        tickSpacing: TickSpacing.Standard,
        defaultFeeRate: newDefaultFeeRate,
      })
    )
      .addSigner(feeAuthorityKeypair)
      .buildAndExecute();

    // Setting the default rate did not change existing plasmapool fee rate
    plasmapool_0 = (await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey)) as PlasmaPoolData;
    assert.equal(plasmapool_0.feeRate, feeTierParams.defaultFeeRate);

    // Newly initialized plasmapools have new default fee rate
    const [tokenMintA, tokenMintB] = await createInOrderMints(ctx);
    const plasmapoolPda = PDAUtil.getPlasmaPool(
      ctx.program.programId,
      plasmapoolsConfigKey,
      tokenMintA,
      tokenMintB,
      TickSpacing.Stable
    );
    const tokenVaultAKeypair = anchor.web3.Keypair.generate();
    const tokenVaultBKeypair = anchor.web3.Keypair.generate();

    const newPoolInitInfo: InitPoolParams = {
      ...poolInitInfo,
      tokenMintA,
      tokenMintB,
      plasmapoolPda,
      tokenVaultAKeypair,
      tokenVaultBKeypair,
      tickSpacing: TickSpacing.Stable,
    };
    await toTx(ctx, PlasmaPoolIx.initializePoolIx(ctx.program, newPoolInitInfo)).buildAndExecute();

    const plasmapool_1 = (await fetcher.getPool(plasmapoolPda.publicKey)) as PlasmaPoolData;
    assert.equal(plasmapool_1.feeRate, newDefaultFeeRate);
  });

  it("fails when default fee rate exceeds max", async () => {
    const { configInitInfo, configKeypairs } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newDefaultFeeRate = 20_000;
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setDefaultFeeRateIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          tickSpacing: TickSpacing.Standard,
          defaultFeeRate: newDefaultFeeRate,
        })
      )
        .addSigner(feeAuthorityKeypair)
        .buildAndExecute(),
      /0x178c/ // FeeRateMaxExceeded
    );
  });

  it("fails when fee tier account has not been initialized", async () => {
    const { configInitInfo, configKeypairs } = generateDefaultConfigParams(ctx);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setDefaultFeeRateIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          tickSpacing: TickSpacing.Standard,
          defaultFeeRate: 500,
        })
      )
        .addSigner(feeAuthorityKeypair)
        .buildAndExecute(),
      /0xbc4/ // AccountNotInitialized
    );
  });

  it("fails when fee authority is not a signer", async () => {
    const { configInitInfo, configKeypairs } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;
    const feeTierPda = PDAUtil.getFeeTier(
      ctx.program.programId,
      configInitInfo.plasmapoolsConfigKeypair.publicKey,
      TickSpacing.Standard
    );

    const newDefaultFeeRate = 1000;
    await assert.rejects(
      program.rpc.setDefaultFeeRate(newDefaultFeeRate, {
        accounts: {
          plasmapoolsConfig: plasmapoolsConfigKey,
          feeTier: feeTierPda.publicKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
        },
      }),
      /Signature verification failed/
    );
  });

  it("fails when invalid fee authority provided", async () => {
    const { configInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const fakeFeeAuthorityKeypair = anchor.web3.Keypair.generate();
    const feeTierPda = PDAUtil.getFeeTier(
      ctx.program.programId,
      configInitInfo.plasmapoolsConfigKeypair.publicKey,
      TickSpacing.Standard
    );

    const newDefaultFeeRate = 1000;
    await assert.rejects(
      program.rpc.setDefaultFeeRate(newDefaultFeeRate, {
        accounts: {
          plasmapoolsConfig: plasmapoolsConfigKey,
          feeTier: feeTierPda.publicKey,
          feeAuthority: fakeFeeAuthorityKeypair.publicKey,
        },
        signers: [fakeFeeAuthorityKeypair],
      }),
      /An address constraint was violated/ // ConstraintAddress
    );
  });
});
