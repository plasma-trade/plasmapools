import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import {
  PlasmaPoolContext,
  AccountFetcher,
  PlasmaPoolsConfigData,
  PlasmaPoolIx,
  InitConfigParams,
  toTx,
} from "../../src";
import { systemTransferTx, ONE_SOL } from "../utils";
import { generateDefaultConfigParams } from "../utils/test-builders";

describe("initialize_config", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  let initializedConfigInfo: InitConfigParams;

  it("successfully init a PlasmaPoolsConfig account", async () => {
    const { configInitInfo } = generateDefaultConfigParams(ctx);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();

    const configAccount = (await fetcher.getConfig(
      configInitInfo.plasmapoolsConfigKeypair.publicKey
    )) as PlasmaPoolsConfigData;

    assert.ok(
      configAccount.collectProtocolFeesAuthority.equals(configInitInfo.collectProtocolFeesAuthority)
    );

    assert.ok(configAccount.feeAuthority.equals(configInitInfo.feeAuthority));

    assert.ok(
      configAccount.rewardEmissionsSuperAuthority.equals(
        configInitInfo.rewardEmissionsSuperAuthority
      )
    );

    assert.equal(configAccount.defaultProtocolFeeRate, configInitInfo.defaultProtocolFeeRate);

    initializedConfigInfo = configInitInfo;
  });

  it("fail on passing in already initialized plasmapool account", async () => {
    let infoWithDupeConfigKey = {
      ...generateDefaultConfigParams(ctx).configInitInfo,
      plasmapoolsConfigKeypair: initializedConfigInfo.plasmapoolsConfigKeypair,
    };
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.initializeConfigIx(ctx.program, infoWithDupeConfigKey)
      ).buildAndExecute(),
      /0x0/
    );
  });

  it("succeeds when funder is different than account paying for transaction fee", async () => {
    const funderKeypair = anchor.web3.Keypair.generate();
    await systemTransferTx(provider, funderKeypair.publicKey, ONE_SOL).buildAndExecute();
    const { configInitInfo } = generateDefaultConfigParams(ctx, funderKeypair.publicKey);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo))
      .addSigner(funderKeypair)
      .buildAndExecute();
  });
});
