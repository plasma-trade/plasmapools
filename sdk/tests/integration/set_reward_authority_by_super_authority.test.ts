import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import { PlasmaPoolContext, AccountFetcher, PlasmaPoolData, PlasmaPoolIx, toTx } from "../../src";
import { TickSpacing } from "../utils";
import { initTestPool } from "../utils/init-utils";

describe("set_reward_authority_by_super_authority", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully set_reward_authority_by_super_authority", async () => {
    const { configKeypairs, poolInitInfo, configInitInfo } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const newAuthorityKeypair = anchor.web3.Keypair.generate();
    await toTx(
      ctx,
      PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
        plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
        plasmapool: poolInitInfo.plasmapoolPda.publicKey,
        rewardEmissionsSuperAuthority:
          configKeypairs.rewardEmissionsSuperAuthorityKeypair.publicKey,
        newRewardAuthority: newAuthorityKeypair.publicKey,
        rewardIndex: 0,
      })
    )
      .addSigner(configKeypairs.rewardEmissionsSuperAuthorityKeypair)
      .buildAndExecute();
    const pool = (await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey)) as PlasmaPoolData;
    assert.ok(pool.rewardInfos[0].authority.equals(newAuthorityKeypair.publicKey));
  });

  it("fails if invalid plasmapool provided", async () => {
    const { configKeypairs, configInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const {
      poolInitInfo: { plasmapoolPda: invalidPool },
    } = await initTestPool(ctx, TickSpacing.Standard);

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: invalidPool.publicKey,
          rewardEmissionsSuperAuthority:
            configKeypairs.rewardEmissionsSuperAuthorityKeypair.publicKey,
          newRewardAuthority: provider.wallet.publicKey,
          rewardIndex: 0,
        })
      )
        .addSigner(configKeypairs.rewardEmissionsSuperAuthorityKeypair)
        .buildAndExecute(),
      /0x7d1/ // A has_one constraint was violated
    );
  });

  it("fails if invalid super authority provided", async () => {
    const { poolInitInfo, configInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const invalidSuperAuthorityKeypair = anchor.web3.Keypair.generate();

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: poolInitInfo.plasmapoolPda.publicKey,
          rewardEmissionsSuperAuthority: invalidSuperAuthorityKeypair.publicKey,
          newRewardAuthority: provider.wallet.publicKey,
          rewardIndex: 0,
        })
      )
        .addSigner(invalidSuperAuthorityKeypair)
        .buildAndExecute(),
      /0x7dc/ // An address constraint was violated
    );
  });

  it("fails if super authority is not a signer", async () => {
    const { configKeypairs, poolInitInfo, configInitInfo } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: poolInitInfo.plasmapoolPda.publicKey,
          rewardEmissionsSuperAuthority:
            configKeypairs.rewardEmissionsSuperAuthorityKeypair.publicKey,
          newRewardAuthority: provider.wallet.publicKey,
          rewardIndex: 0,
        })
      ).buildAndExecute(),
      /Signature verification failed/
    );
  });

  it("fails on invalid reward index", async () => {
    const { configKeypairs, poolInitInfo, configInitInfo } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );

    assert.throws(() => {
      toTx(
        ctx,
        PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: poolInitInfo.plasmapoolPda.publicKey,
          rewardEmissionsSuperAuthority:
            configKeypairs.rewardEmissionsSuperAuthorityKeypair.publicKey,
          newRewardAuthority: provider.wallet.publicKey,
          rewardIndex: -1,
        })
      )
        .addSigner(configKeypairs.rewardEmissionsSuperAuthorityKeypair)
        .buildAndExecute();
    }, /out of range/);

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setRewardAuthorityBySuperAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: poolInitInfo.plasmapoolPda.publicKey,
          rewardEmissionsSuperAuthority:
            configKeypairs.rewardEmissionsSuperAuthorityKeypair.publicKey,
          newRewardAuthority: provider.wallet.publicKey,
          rewardIndex: 200,
        })
      )
        .addSigner(configKeypairs.rewardEmissionsSuperAuthorityKeypair)
        .buildAndExecute(),
      /0x178a/ // InvalidRewardIndex
    );
  });
});
