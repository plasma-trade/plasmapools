import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import {
  PlasmaPoolContext,
  AccountFetcher,
  PlasmaPoolsConfigData,
  PlasmaPoolIx,
  toTx,
} from "../../src";
import { generateDefaultConfigParams } from "../utils/test-builders";

describe("set_fee_authority", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully set_fee_authority", async () => {
    const {
      configInitInfo,
      configKeypairs: { feeAuthorityKeypair },
    } = generateDefaultConfigParams(ctx);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();
    const newAuthorityKeypair = anchor.web3.Keypair.generate();
    await toTx(
      ctx,
      PlasmaPoolIx.setFeeAuthorityIx(ctx.program, {
        plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
        feeAuthority: feeAuthorityKeypair.publicKey,
        newFeeAuthority: newAuthorityKeypair.publicKey,
      })
    )
      .addSigner(feeAuthorityKeypair)
      .buildAndExecute();
    const config = (await fetcher.getConfig(
      configInitInfo.plasmapoolsConfigKeypair.publicKey
    )) as PlasmaPoolsConfigData;
    assert.ok(config.feeAuthority.equals(newAuthorityKeypair.publicKey));
  });

  it("fails if current fee_authority is not a signer", async () => {
    const {
      configInitInfo,
      configKeypairs: { feeAuthorityKeypair },
    } = generateDefaultConfigParams(ctx);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setFeeAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          newFeeAuthority: provider.wallet.publicKey,
        })
      ).buildAndExecute(),
      /Signature verification failed/
    );
  });

  it("fails if invalid fee_authority provided", async () => {
    const { configInitInfo } = generateDefaultConfigParams(ctx);
    await toTx(ctx, PlasmaPoolIx.initializeConfigIx(ctx.program, configInitInfo)).buildAndExecute();

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setFeeAuthorityIx(ctx.program, {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          feeAuthority: provider.wallet.publicKey,
          newFeeAuthority: provider.wallet.publicKey,
        })
      ).buildAndExecute(),
      /0x7dc/ // An address constraint was violated
    );
  });
});
