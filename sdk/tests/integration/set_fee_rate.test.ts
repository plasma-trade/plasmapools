import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import { PlasmaPoolContext, AccountFetcher, PlasmaPoolData, PlasmaPoolIx, toTx } from "../../src";
import { TickSpacing } from "../utils";
import { initTestPool } from "../utils/init-utils";
import { generateDefaultConfigParams } from "../utils/test-builders";

describe("set_fee_rate", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully sets_fee_rate", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs, feeTierParams } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newFeeRate = 50;

    let plasmapool = (await fetcher.getPool(plasmapoolKey, true)) as PlasmaPoolData;

    assert.equal(plasmapool.feeRate, feeTierParams.defaultFeeRate);

    await program.rpc.setFeeRate(newFeeRate, {
      accounts: {
        plasmapoolsConfig: plasmapoolsConfigKey,
        plasmapool: plasmapoolKey,
        feeAuthority: feeAuthorityKeypair.publicKey,
      },
      signers: [feeAuthorityKeypair],
    });

    plasmapool = (await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey, true)) as PlasmaPoolData;
    assert.equal(plasmapool.feeRate, newFeeRate);
  });

  it("fails when fee rate exceeds max", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newFeeRate = 20_000;
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setFeeRateIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          feeRate: newFeeRate,
        })
      )
        .addSigner(configKeypairs.feeAuthorityKeypair)
        .buildAndExecute(),
      /0x178c/ // FeeRateMaxExceeded
    );
  });

  it("fails when fee authority is not signer", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newFeeRate = 1000;
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setFeeRateIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          feeRate: newFeeRate,
        })
      ).buildAndExecute(),
      /Signature verification failed/
    );
  });

  it("fails when plasmapool and plasmapools config don't match", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const { configInitInfo: otherConfigInitInfo } = generateDefaultConfigParams(ctx);
    await toTx(
      ctx,
      PlasmaPoolIx.initializeConfigIx(ctx.program, otherConfigInitInfo)
    ).buildAndExecute();

    const newFeeRate = 1000;
    await assert.rejects(
      ctx.program.rpc.setFeeRate(newFeeRate, {
        accounts: {
          plasmapoolsConfig: otherConfigInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
        },
        signers: [configKeypairs.feeAuthorityKeypair],
      }),
      /A has_one constraint was violated/ // ConstraintHasOne
    );
  });

  it("fails when fee authority is invalid", async () => {
    const { poolInitInfo, configInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;

    const fakeAuthorityKeypair = anchor.web3.Keypair.generate();

    const newFeeRate = 1000;
    await assert.rejects(
      ctx.program.rpc.setFeeRate(newFeeRate, {
        accounts: {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolKey,
          feeAuthority: fakeAuthorityKeypair.publicKey,
        },
        signers: [fakeAuthorityKeypair],
      }),
      /An address constraint was violated/ // ConstraintAddress
    );
  });
});
