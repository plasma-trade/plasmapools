import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import { u64 } from "@solana/spl-token";
import Decimal from "decimal.js";
import {
  PlasmaPoolContext,
  AccountFetcher,
  PlasmaPoolData,
  PlasmaPoolIx,
  PDAUtil,
  toTx,
} from "../../src";
import { TickSpacing, ZERO_BN, createTokenAccount, getTokenBalance } from "../utils";
import { PlasmaPoolTestFixture } from "../utils/fixture";
import { initTestPool } from "../utils/init-utils";
import { MathUtil } from "@plasmatrade-so/common-sdk";

describe("collect_protocol_fees", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully collects fees", async () => {
    // In same tick array - start index 22528
    const tickLowerIndex = 29440;
    const tickUpperIndex = 33536;

    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [{ tickLowerIndex, tickUpperIndex, liquidityAmount: new u64(10_000_000) }],
    });
    const {
      poolInitInfo: {
        plasmapoolPda,
        tokenVaultAKeypair,
        tokenVaultBKeypair,
        tokenMintA,
        tokenMintB,
      },
      configKeypairs: { feeAuthorityKeypair, collectProtocolFeesAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair: plasmapoolsConfigKeypair },
      tokenAccountA,
      tokenAccountB,
      positions,
    } = fixture.getInfos();

    await toTx(
      ctx,
      PlasmaPoolIx.setProtocolFeeRateIx(ctx.program, {
        plasmapool: plasmapoolPda.publicKey,
        plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
        feeAuthority: feeAuthorityKeypair.publicKey,
        protocolFeeRate: 2500,
      })
    )
      .addSigner(feeAuthorityKeypair)
      .buildAndExecute();

    const poolBefore = (await fetcher.getPool(plasmapoolPda.publicKey, true)) as PlasmaPoolData;
    assert.ok(poolBefore?.protocolFeeOwedA.eq(ZERO_BN));
    assert.ok(poolBefore?.protocolFeeOwedB.eq(ZERO_BN));

    const tickArrayPda = positions[0].tickArrayLower;

    const oraclePda = PDAUtil.getOracle(ctx.program.programId, plasmapoolPda.publicKey);

    // Accrue fees in token A
    await toTx(
      ctx,
      PlasmaPoolIx.swapIx(ctx.program, {
        amount: new u64(200_000),
        otherAmountThreshold: ZERO_BN,
        sqrtPriceLimit: MathUtil.toX64(new Decimal(4)),
        amountSpecifiedIsInput: true,
        aToB: true,
        plasmapool: plasmapoolPda.publicKey,
        tokenAuthority: ctx.wallet.publicKey,
        tokenOwnerAccountA: tokenAccountA,
        tokenVaultA: tokenVaultAKeypair.publicKey,
        tokenOwnerAccountB: tokenAccountB,
        tokenVaultB: tokenVaultBKeypair.publicKey,
        tickArray0: tickArrayPda,
        tickArray1: tickArrayPda,
        tickArray2: tickArrayPda,
        oracle: oraclePda.publicKey,
      })
    ).buildAndExecute();

    // Accrue fees in token B
    await toTx(
      ctx,
      PlasmaPoolIx.swapIx(ctx.program, {
        amount: new u64(200_000),
        otherAmountThreshold: ZERO_BN,
        sqrtPriceLimit: MathUtil.toX64(new Decimal(5)),
        amountSpecifiedIsInput: true,
        aToB: false,
        plasmapool: plasmapoolPda.publicKey,
        tokenAuthority: ctx.wallet.publicKey,
        tokenOwnerAccountA: tokenAccountA,
        tokenVaultA: tokenVaultAKeypair.publicKey,
        tokenOwnerAccountB: tokenAccountB,
        tokenVaultB: tokenVaultBKeypair.publicKey,
        tickArray0: tickArrayPda,
        tickArray1: tickArrayPda,
        tickArray2: tickArrayPda,
        oracle: oraclePda.publicKey,
      })
    ).buildAndExecute();

    const poolAfter = (await fetcher.getPool(plasmapoolPda.publicKey, true)) as PlasmaPoolData;
    assert.ok(poolAfter?.protocolFeeOwedA.eq(new u64(150)));
    assert.ok(poolAfter?.protocolFeeOwedB.eq(new u64(150)));

    const destA = await createTokenAccount(provider, tokenMintA, provider.wallet.publicKey);
    const destB = await createTokenAccount(provider, tokenMintB, provider.wallet.publicKey);

    await toTx(
      ctx,
      PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
        plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
        plasmapool: plasmapoolPda.publicKey,
        collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
        tokenVaultA: tokenVaultAKeypair.publicKey,
        tokenVaultB: tokenVaultBKeypair.publicKey,
        tokenOwnerAccountA: destA,
        tokenOwnerAccountB: destB,
      })
    )
      .addSigner(collectProtocolFeesAuthorityKeypair)
      .buildAndExecute();

    const balanceDestA = await getTokenBalance(provider, destA);
    const balanceDestB = await getTokenBalance(provider, destB);
    assert.equal(balanceDestA, "150");
    assert.equal(balanceDestB, "150");
    assert.ok(poolBefore?.protocolFeeOwedA.eq(ZERO_BN));
    assert.ok(poolBefore?.protocolFeeOwedB.eq(ZERO_BN));
  });

  it("fails to collect fees without the authority's signature", async () => {
    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [
        { tickLowerIndex: 29440, tickUpperIndex: 33536, liquidityAmount: new u64(10_000_000) },
      ],
    });
    const {
      poolInitInfo: { plasmapoolPda, tokenVaultAKeypair, tokenVaultBKeypair },
      configKeypairs: { collectProtocolFeesAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair },
      tokenAccountA,
      tokenAccountB,
    } = fixture.getInfos();

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: tokenAccountB,
        })
      ).buildAndExecute(),
      /Signature verification failed/
    );
  });

  it("fails when collect_protocol_fees_authority is invalid", async () => {
    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [
        { tickLowerIndex: 29440, tickUpperIndex: 33536, liquidityAmount: new u64(10_000_000) },
      ],
    });
    const {
      poolInitInfo: { plasmapoolPda, tokenVaultAKeypair, tokenVaultBKeypair },
      configKeypairs: { rewardEmissionsSuperAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair },
      tokenAccountA,
      tokenAccountB,
    } = fixture.getInfos();

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: rewardEmissionsSuperAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: tokenAccountB,
        })
      )
        .addSigner(rewardEmissionsSuperAuthorityKeypair)
        .buildAndExecute(),
      /0x7dc/ // ConstraintAddress
    );
  });

  it("fails when plasmapool does not match config", async () => {
    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [
        { tickLowerIndex: 29440, tickUpperIndex: 33536, liquidityAmount: new u64(10_000_000) },
      ],
    });
    const {
      poolInitInfo: { tokenVaultAKeypair, tokenVaultBKeypair },
      configKeypairs: { collectProtocolFeesAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair },
      tokenAccountA,
      tokenAccountB,
    } = fixture.getInfos();
    const {
      poolInitInfo: { plasmapoolPda: plasmapoolPda2 },
    } = await initTestPool(ctx, tickSpacing);

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolPda2.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: tokenAccountB,
        })
      )
        .addSigner(collectProtocolFeesAuthorityKeypair)
        .buildAndExecute(),
      /0x7d1/ // ConstraintHasOne
    );
  });

  it("fails when vaults do not match plasmapool vaults", async () => {
    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [
        { tickLowerIndex: 29440, tickUpperIndex: 33536, liquidityAmount: new u64(10_000_000) },
      ],
    });
    const {
      poolInitInfo: {
        plasmapoolPda,
        tokenVaultAKeypair,
        tokenVaultBKeypair,
        tokenMintA,
        tokenMintB,
      },
      configKeypairs: { collectProtocolFeesAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair: plasmapoolsConfigKeypair },
      tokenAccountA,
      tokenAccountB,
    } = fixture.getInfos();

    const fakeVaultA = await createTokenAccount(provider, tokenMintA, provider.wallet.publicKey);
    const fakeVaultB = await createTokenAccount(provider, tokenMintB, provider.wallet.publicKey);

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: fakeVaultA,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: tokenAccountB,
        })
      )
        .addSigner(collectProtocolFeesAuthorityKeypair)
        .buildAndExecute(),
      /0x7dc/ // ConstraintAddress
    );

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: fakeVaultB,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: tokenAccountB,
        })
      )
        .addSigner(collectProtocolFeesAuthorityKeypair)
        .buildAndExecute(),
      /0x7dc/ // ConstraintAddress
    );
  });

  it("fails when destination mints do not match plasmapool mints", async () => {
    const tickSpacing = TickSpacing.Standard;
    const fixture = await new PlasmaPoolTestFixture(ctx).init({
      tickSpacing,
      positions: [
        { tickLowerIndex: 29440, tickUpperIndex: 33536, liquidityAmount: new u64(10_000_000) },
      ],
    });
    const {
      poolInitInfo: {
        plasmapoolPda,
        tokenVaultAKeypair,
        tokenVaultBKeypair,
        tokenMintA,
        tokenMintB,
      },
      configKeypairs: { collectProtocolFeesAuthorityKeypair },
      configInitInfo: { plasmapoolsConfigKeypair: plasmapoolsConfigKepair },
      tokenAccountA,
      tokenAccountB,
    } = fixture.getInfos();

    const invalidDestA = await createTokenAccount(provider, tokenMintB, provider.wallet.publicKey);
    const invalidDestB = await createTokenAccount(provider, tokenMintA, provider.wallet.publicKey);

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKepair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: invalidDestA,
          tokenOwnerAccountB: tokenAccountB,
        })
      )
        .addSigner(collectProtocolFeesAuthorityKeypair)
        .buildAndExecute(),
      /0x7d3/ // ConstraintRaw
    );

    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.collectProtocolFeesIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKepair.publicKey,
          plasmapool: plasmapoolPda.publicKey,
          collectProtocolFeesAuthority: collectProtocolFeesAuthorityKeypair.publicKey,
          tokenVaultA: tokenVaultAKeypair.publicKey,
          tokenVaultB: tokenVaultBKeypair.publicKey,
          tokenOwnerAccountA: tokenAccountA,
          tokenOwnerAccountB: invalidDestB,
        })
      )
        .addSigner(collectProtocolFeesAuthorityKeypair)
        .buildAndExecute(),
      /0x7d3/ // ConstraintRaw
    );
  });
});
