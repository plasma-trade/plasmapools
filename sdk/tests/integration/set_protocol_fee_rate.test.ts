import * as assert from "assert";
import * as anchor from "@project-serum/anchor";
import { PlasmaPoolContext, AccountFetcher, PlasmaPoolData, PlasmaPoolIx, toTx } from "../../src";
import { TickSpacing } from "../utils";
import { initTestPool } from "../utils/init-utils";
import { generateDefaultConfigParams } from "../utils/test-builders";

describe("set_protocol_fee_rate", () => {
  const provider = anchor.Provider.local();
  anchor.setProvider(anchor.Provider.env());
  const program = anchor.workspace.PlasmaPool;
  const ctx = PlasmaPoolContext.fromWorkspace(provider, program);
  const fetcher = new AccountFetcher(ctx.connection);

  it("successfully sets_protocol_fee_rate", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newProtocolFeeRate = 50;

    let plasmapool = (await fetcher.getPool(plasmapoolKey, true)) as PlasmaPoolData;

    assert.equal(plasmapool.protocolFeeRate, configInitInfo.defaultProtocolFeeRate);

    await program.rpc.setProtocolFeeRate(newProtocolFeeRate, {
      accounts: {
        plasmapoolsConfig: plasmapoolsConfigKey,
        plasmapool: plasmapoolKey,
        feeAuthority: feeAuthorityKeypair.publicKey,
      },
      signers: [feeAuthorityKeypair],
    });

    plasmapool = (await fetcher.getPool(poolInitInfo.plasmapoolPda.publicKey, true)) as PlasmaPoolData;
    assert.equal(plasmapool.protocolFeeRate, newProtocolFeeRate);
  });

  it("fails when protocol fee rate exceeds max", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newProtocolFeeRate = 3_000;
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setProtocolFeeRateIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          protocolFeeRate: newProtocolFeeRate,
        })
      )
        .addSigner(configKeypairs.feeAuthorityKeypair)
        .buildAndExecute(),
      /0x178d/ // ProtocolFeeRateMaxExceeded
    );
  });

  it("fails when fee authority is not signer", async () => {
    const { poolInitInfo, configInitInfo, configKeypairs } = await initTestPool(
      ctx,
      TickSpacing.Standard
    );
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const plasmapoolsConfigKey = configInitInfo.plasmapoolsConfigKeypair.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const newProtocolFeeRate = 1000;
    await assert.rejects(
      toTx(
        ctx,
        PlasmaPoolIx.setProtocolFeeRateIx(ctx.program, {
          plasmapoolsConfig: plasmapoolsConfigKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
          protocolFeeRate: newProtocolFeeRate,
        })
      ).buildAndExecute(),
      /Signature verification failed/
    );
  });

  it("fails when plasmapool and plasmapools config don't match", async () => {
    const { poolInitInfo, configKeypairs } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const feeAuthorityKeypair = configKeypairs.feeAuthorityKeypair;

    const { configInitInfo: otherConfigInitInfo } = generateDefaultConfigParams(ctx);
    await toTx(
      ctx,
      PlasmaPoolIx.initializeConfigIx(ctx.program, otherConfigInitInfo)
    ).buildAndExecute();

    const newProtocolFeeRate = 1000;
    await assert.rejects(
      ctx.program.rpc.setProtocolFeeRate(newProtocolFeeRate, {
        accounts: {
          plasmapoolsConfig: otherConfigInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolKey,
          feeAuthority: feeAuthorityKeypair.publicKey,
        },
        signers: [configKeypairs.feeAuthorityKeypair],
      }),
      /A has_one constraint was violated/ // ConstraintHasOne
    );
  });

  it("fails when fee authority is invalid", async () => {
    const { poolInitInfo, configInitInfo } = await initTestPool(ctx, TickSpacing.Standard);
    const plasmapoolKey = poolInitInfo.plasmapoolPda.publicKey;
    const fakeAuthorityKeypair = anchor.web3.Keypair.generate();

    const newProtocolFeeRate = 1000;
    await assert.rejects(
      ctx.program.rpc.setProtocolFeeRate(newProtocolFeeRate, {
        accounts: {
          plasmapoolsConfig: configInitInfo.plasmapoolsConfigKeypair.publicKey,
          plasmapool: plasmapoolKey,
          feeAuthority: fakeAuthorityKeypair.publicKey,
        },
        signers: [fakeAuthorityKeypair],
      }),
      /An address constraint was violated/ // ConstraintAddress
    );
  });
});
