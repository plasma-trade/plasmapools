import * as assert from "assert";
import { PoolUtil, SwapDirection, TokenType } from "../../../../src";
import { testPlasmaPoolData } from "../../../utils/testDataTypes";
import { Keypair } from "@solana/web3.js";

describe("PoolUtils tests", () => {
  describe("getTokenType", () => {
    it("Token is tokenA", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getTokenType(plasmapoolData, plasmapoolData.tokenMintA);
      assert.equal(result, TokenType.TokenA);
    });

    it("Token is tokenB", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getTokenType(plasmapoolData, plasmapoolData.tokenMintB);
      assert.equal(result, TokenType.TokenB);
    });

    it("Token is some other token", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getTokenType(plasmapoolData, Keypair.generate().publicKey);
      assert.ok(result === undefined);
    });
  });

  describe("getSwapDirection", () => {
    it("SwapToken is tokenA and is an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, plasmapoolData.tokenMintA, true);
      assert.equal(result, SwapDirection.AtoB);
    });

    it("SwapToken is tokenB and is an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, plasmapoolData.tokenMintB, true);
      assert.equal(result, SwapDirection.BtoA);
    });

    it("SwapToken is tokenA and is not an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, plasmapoolData.tokenMintA, false);
      assert.equal(result, SwapDirection.BtoA);
    });

    it("SwapToken is tokenB and is not an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, plasmapoolData.tokenMintB, false);
      assert.equal(result, SwapDirection.AtoB);
    });

    it("SwapToken is a random mint and is an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, Keypair.generate().publicKey, true);
      assert.equal(result, undefined);
    });

    it("SwapToken is a random mint and is not an input", async () => {
      const plasmapoolData = testPlasmaPoolData;
      const result = PoolUtil.getSwapDirection(plasmapoolData, Keypair.generate().publicKey, false);
      assert.equal(result, undefined);
    });
  });
});
