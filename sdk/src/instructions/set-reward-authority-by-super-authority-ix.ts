import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to update the reward authority at a particular rewardIndex on a PlasmaPool.
 *
 * @category Instruction Types
 * @param plasmapool - PublicKey for the plasmapool to update. This plasmapool has to be part of the provided PlasmaPoolsConfig space.
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param rewardIndex - The reward index that we'd like to update. (0 <= index <= NUM_REWARDS).
 * @param rewardEmissionsSuperAuthority - The current rewardEmissionsSuperAuthority in the PlasmaPoolsConfig
 * @param newRewardAuthority - The new rewardAuthority in the PlasmaPool at the rewardIndex
 */
export type SetRewardAuthorityBySuperAuthorityParams = {
  plasmapool: PublicKey;
  plasmapoolsConfig: PublicKey;
  rewardIndex: number;
  rewardEmissionsSuperAuthority: PublicKey;
  newRewardAuthority: PublicKey;
};

/**
 * Set the plasmapool reward authority at the provided `reward_index`.
 * Only the current reward super authority has permission to invoke this instruction.
 *
 * #### Special Errors
 * - `InvalidRewardIndex` - If the provided reward index doesn't match the lowest uninitialized index in this pool,
 *                          or exceeds NUM_REWARDS.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetRewardAuthorityParams object
 * @returns - Instruction to perform the action.
 */
export function setRewardAuthorityBySuperAuthorityIx(
  program: Program<PlasmaPool>,
  params: SetRewardAuthorityBySuperAuthorityParams
): Instruction {
  const {
    plasmapoolsConfig,
    plasmapool,
    rewardEmissionsSuperAuthority,
    newRewardAuthority,
    rewardIndex,
  } = params;

  const ix = program.instruction.setRewardAuthorityBySuperAuthority(rewardIndex, {
    accounts: {
      plasmapoolsConfig,
      plasmapool,
      rewardEmissionsSuperAuthority,
      newRewardAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
