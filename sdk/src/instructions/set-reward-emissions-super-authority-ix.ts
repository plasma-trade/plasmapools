import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to set rewards emissions for a reward in a PlasmaPool
 *
 * @category Instruction Types
 * @param plasmapoolsConfig - PublicKey for the PlasmaPoolsConfig that we want to update.
 * @param rewardEmissionsSuperAuthority - Current reward emission super authority in this PlasmaPoolsConfig
 * @param newRewardEmissionsSuperAuthority - New reward emission super authority for this PlasmaPoolsConfig
 */
export type SetRewardEmissionsSuperAuthorityParams = {
  plasmapoolsConfig: PublicKey;
  rewardEmissionsSuperAuthority: PublicKey;
  newRewardEmissionsSuperAuthority: PublicKey;
};

/**
 * Set the plasmapool reward super authority for a PlasmaPoolsConfig
 * Only the current reward super authority has permission to invoke this instruction.
 * This instruction will not change the authority on any `PlasmaPoolRewardInfo` plasmapool rewards.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetRewardEmissionsSuperAuthorityParams object
 * @returns - Instruction to perform the action.
 */
export function setRewardEmissionsSuperAuthorityIx(
  program: Program<PlasmaPool>,
  params: SetRewardEmissionsSuperAuthorityParams
): Instruction {
  const { plasmapoolsConfig, rewardEmissionsSuperAuthority, newRewardEmissionsSuperAuthority } =
    params;

  const ix = program.instruction.setRewardEmissionsSuperAuthority({
    accounts: {
      plasmapoolsConfig,
      rewardEmissionsSuperAuthority: rewardEmissionsSuperAuthority,
      newRewardEmissionsSuperAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
