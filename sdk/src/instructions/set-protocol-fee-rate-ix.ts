import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to set fee rate for a PlasmaPool.
 *
 * @category Instruction Types
 * @param plasmapool - PublicKey for the plasmapool to update. This plasmapool has to be part of the provided PlasmaPoolsConfig space.
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param feeAuthority - Authority authorized in the PlasmaPoolsConfig to set default fee rates.
 * @param protocolFeeRate - The new default protocol fee rate for this pool. Stored as a basis point of the total fees collected by feeRate.
 */
export type SetProtocolFeeRateParams = {
  plasmapool: PublicKey;
  plasmapoolsConfig: PublicKey;
  feeAuthority: PublicKey;
  protocolFeeRate: number;
};

/**
 * Sets the protocol fee rate for a PlasmaPool.
 * Only the current fee authority has permission to invoke this instruction.
 *
 * #### Special Errors
 * - `ProtocolFeeRateMaxExceeded` - If the provided default_protocol_fee_rate exceeds MAX_PROTOCOL_FEE_RATE.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetFeeRateParams object
 * @returns - Instruction to perform the action.
 */
export function setProtocolFeeRateIx(
  program: Program<PlasmaPool>,
  params: SetProtocolFeeRateParams
): Instruction {
  const { plasmapoolsConfig, plasmapool, feeAuthority, protocolFeeRate } = params;

  const ix = program.instruction.setProtocolFeeRate(protocolFeeRate, {
    accounts: {
      plasmapoolsConfig,
      plasmapool,
      feeAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
