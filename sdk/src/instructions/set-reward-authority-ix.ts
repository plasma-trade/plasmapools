import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to update the reward authority at a particular rewardIndex on a PlasmaPool.
 *
 * @category Instruction Types
 * @param plasmapool - PublicKey for the plasmapool to update.
 * @param rewardIndex - The reward index that we'd like to update. (0 <= index <= NUM_REWARDS).
 * @param rewardAuthority - The current rewardAuthority in the PlasmaPool at the rewardIndex
 * @param newRewardAuthority - The new rewardAuthority in the PlasmaPool at the rewardIndex
 */
export type SetRewardAuthorityParams = {
  plasmapool: PublicKey;
  rewardIndex: number;
  rewardAuthority: PublicKey;
  newRewardAuthority: PublicKey;
};

/**
 * Set the plasmapool reward authority at the provided `reward_index`.
 * Only the current reward authority for this reward index has permission to invoke this instruction.
 *
 * #### Special Errors
 * - `InvalidRewardIndex` - If the provided reward index doesn't match the lowest uninitialized index in this pool,
 *                          or exceeds NUM_REWARDS.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetRewardAuthorityParams object
 * @returns - Instruction to perform the action.
 */
export function setRewardAuthorityIx(
  program: Program<PlasmaPool>,
  params: SetRewardAuthorityParams
): Instruction {
  const { plasmapool, rewardAuthority, newRewardAuthority, rewardIndex } = params;
  const ix = program.instruction.setRewardAuthority(rewardIndex, {
    accounts: {
      plasmapool,
      rewardAuthority,
      newRewardAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
