import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to set fee rate for a PlasmaPool.
 *
 * @category Instruction Types
 * @param plasmapool - PublicKey for the plasmapool to update. This plasmapool has to be part of the provided PlasmaPoolsConfig space.
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param feeAuthority - Authority authorized in the PlasmaPoolsConfig to set default fee rates.
 * @param feeRate - The new fee rate for this fee-tier. Stored as a hundredths of a basis point.
 */
export type SetFeeRateParams = {
  plasmapool: PublicKey;
  plasmapoolsConfig: PublicKey;
  feeAuthority: PublicKey;
  feeRate: number;
};

/**
 * Sets the fee rate for a PlasmaPool.
 * Only the current fee authority has permission to invoke this instruction.
 *
 * #### Special Errors
 * - `FeeRateMaxExceeded` - If the provided fee_rate exceeds MAX_FEE_RATE.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetFeeRateParams object
 * @returns - Instruction to perform the action.
 */
export function setFeeRateIx(program: Program<PlasmaPool>, params: SetFeeRateParams): Instruction {
  const { plasmapoolsConfig, plasmapool, feeAuthority, feeRate } = params;

  const ix = program.instruction.setFeeRate(feeRate, {
    accounts: {
      plasmapoolsConfig,
      plasmapool,
      feeAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
