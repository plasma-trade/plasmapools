import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to set the fee authority in a PlasmaPoolsConfig
 *
 * @category Instruction Types
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param feeAuthority - The current feeAuthority in the PlasmaPoolsConfig
 * @param newFeeAuthority - The new feeAuthority in the PlasmaPoolsConfig
 */
export type SetFeeAuthorityParams = {
  plasmapoolsConfig: PublicKey;
  feeAuthority: PublicKey;
  newFeeAuthority: PublicKey;
};

/**
 * Sets the fee authority for a PlasmaPoolsConfig.
 * The fee authority can set the fee & protocol fee rate for individual pools or set the default fee rate for newly minted pools.
 * Only the current fee authority has permission to invoke this instruction.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetFeeAuthorityParams object
 * @returns - Instruction to perform the action.
 */
export function setFeeAuthorityIx(
  program: Program<PlasmaPool>,
  params: SetFeeAuthorityParams
): Instruction {
  const { plasmapoolsConfig, feeAuthority, newFeeAuthority } = params;

  const ix = program.instruction.setFeeAuthority({
    accounts: {
      plasmapoolsConfig,
      feeAuthority,
      newFeeAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
