import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to set the collect fee authority in a PlasmaPoolsConfig
 *
 * @category Instruction Types
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param collectProtocolFeesAuthority - The current collectProtocolFeesAuthority in the PlasmaPoolsConfig
 * @param newCollectProtocolFeesAuthority - The new collectProtocolFeesAuthority in the PlasmaPoolsConfig
 */
export type SetCollectProtocolFeesAuthorityParams = {
  plasmapoolsConfig: PublicKey;
  collectProtocolFeesAuthority: PublicKey;
  newCollectProtocolFeesAuthority: PublicKey;
};

/**
 * Sets the fee authority to collect protocol fees for a PlasmaPoolsConfig.
 * Only the current collect protocol fee authority has permission to invoke this instruction.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - SetCollectProtocolFeesAuthorityParams object
 * @returns - Instruction to perform the action.
 */
export function setCollectProtocolFeesAuthorityIx(
  program: Program<PlasmaPool>,
  params: SetCollectProtocolFeesAuthorityParams
): Instruction {
  const { plasmapoolsConfig, collectProtocolFeesAuthority, newCollectProtocolFeesAuthority } =
    params;

  const ix = program.instruction.setCollectProtocolFeesAuthority({
    accounts: {
      plasmapoolsConfig,
      collectProtocolFeesAuthority,
      newCollectProtocolFeesAuthority,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
