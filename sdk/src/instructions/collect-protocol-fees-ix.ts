import { Program } from "@project-serum/anchor";
import { PlasmaPool } from "../artifacts/plasmapool";
import { TOKEN_PROGRAM_ID } from "@solana/spl-token";
import { Instruction } from "@plasmatrade-so/common-sdk";
import { PublicKey } from "@solana/web3.js";

/**
 * Parameters to collect protocol fees for a PlasmaPool
 *
 * @category Instruction Types
 * @param plasmapoolsConfig - The public key for the PlasmaPoolsConfig this pool is initialized in
 * @param plasmapool - PublicKey for the plasmapool that the position will be opened for.
 * @param tokenVaultA - PublicKey for the tokenA vault for this plasmapool.
 * @param tokenVaultB - PublicKey for the tokenB vault for this plasmapool.
 * @param tokenOwnerAccountA - PublicKey for the associated token account for tokenA in the collection wallet
 * @param tokenOwnerAccountB - PublicKey for the associated token account for tokenA in the collection wallet
 * @param collectProtocolFeesAuthority - assigned authority in the PlasmaPoolsConfig that can collect protocol fees
 */
export type CollectProtocolFeesParams = {
  plasmapoolsConfig: PublicKey;
  plasmapool: PublicKey;
  tokenVaultA: PublicKey;
  tokenVaultB: PublicKey;
  tokenOwnerAccountA: PublicKey;
  tokenOwnerAccountB: PublicKey;
  collectProtocolFeesAuthority: PublicKey;
};

/**
 * Collect protocol fees accrued in this PlasmaPool.
 *
 * @category Instructions
 * @param context - Context object containing services required to generate the instruction
 * @param params - CollectProtocolFeesParams object
 * @returns - Instruction to perform the action.
 */
export function collectProtocolFeesIx(
  program: Program<PlasmaPool>,
  params: CollectProtocolFeesParams
): Instruction {
  const {
    plasmapoolsConfig,
    plasmapool,
    collectProtocolFeesAuthority,
    tokenVaultA,
    tokenVaultB,
    tokenOwnerAccountA: tokenDestinationA,
    tokenOwnerAccountB: tokenDestinationB,
  } = params;

  const ix = program.instruction.collectProtocolFees({
    accounts: {
      plasmapoolsConfig,
      plasmapool,
      collectProtocolFeesAuthority,
      tokenVaultA,
      tokenVaultB,
      tokenDestinationA,
      tokenDestinationB,
      tokenProgram: TOKEN_PROGRAM_ID,
    },
  });

  return {
    instructions: [ix],
    cleanupInstructions: [],
    signers: [],
  };
}
