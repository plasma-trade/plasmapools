import { TransactionBuilder, Instruction } from "@plasmatrade-so/common-sdk";
import { PlasmaPoolContext } from "../../context";

export function toTx(ctx: PlasmaPoolContext, ix: Instruction): TransactionBuilder {
  return new TransactionBuilder(ctx.provider).addInstruction(ix);
}
