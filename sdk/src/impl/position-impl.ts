import {
  AddressUtil,
  deriveATA,
  resolveOrCreateATAs,
  TransactionBuilder,
} from "@plasmatrade-so/common-sdk";
import { Address } from "@project-serum/anchor";
import { PlasmaPoolContext } from "../context";
import {
  IncreaseLiquidityInput,
  DecreaseLiquidityInput,
  increaseLiquidityIx,
  decreaseLiquidityIx,
} from "../instructions";
import { PositionData } from "../types/public";
import { Position } from "../plasmapool-client";
import { PublicKey } from "@solana/web3.js";
import { AccountFetcher } from "../network/public";
import { PDAUtil, TickUtil, toTx } from "../utils/public";

export class PositionImpl implements Position {
  private data: PositionData;
  constructor(
    readonly ctx: PlasmaPoolContext,
    readonly fetcher: AccountFetcher,
    readonly address: PublicKey,
    data: PositionData
  ) {
    this.data = data;
  }

  getAddress(): PublicKey {
    return this.address;
  }

  getData(): PositionData {
    return this.data;
  }

  async refreshData() {
    await this.refresh();
    return this.data;
  }

  async increaseLiquidity(
    liquidityInput: IncreaseLiquidityInput,
    sourceWallet?: Address,
    positionWallet?: Address
  ) {
    const sourceWalletKey = sourceWallet
      ? AddressUtil.toPubKey(sourceWallet)
      : this.ctx.wallet.publicKey;
    const positionWalletKey = positionWallet
      ? AddressUtil.toPubKey(positionWallet)
      : this.ctx.wallet.publicKey;

    const plasmapool = await this.fetcher.getPool(this.data.plasmapool, true);
    if (!plasmapool) {
      throw new Error("Unable to fetch plasmapool for this position.");
    }

    const txBuilder = new TransactionBuilder(this.ctx.provider);
    const tokenOwnerAccountA = await deriveATA(sourceWalletKey, plasmapool.tokenMintA);
    const tokenOwnerAccountB = await deriveATA(sourceWalletKey, plasmapool.tokenMintB);
    const positionTokenAccount = await deriveATA(positionWalletKey, this.data.positionMint);

    const increaseIx = increaseLiquidityIx(this.ctx.program, {
      ...liquidityInput,
      plasmapool: this.data.plasmapool,
      position: this.address,
      positionTokenAccount,
      tokenOwnerAccountA,
      tokenOwnerAccountB,
      tokenVaultA: plasmapool.tokenVaultA,
      tokenVaultB: plasmapool.tokenVaultB,
      tickArrayLower: PDAUtil.getTickArray(
        this.ctx.program.programId,
        this.data.plasmapool,
        TickUtil.getStartTickIndex(this.data.tickLowerIndex, plasmapool.tickSpacing)
      ).publicKey,
      tickArrayUpper: PDAUtil.getTickArray(
        this.ctx.program.programId,
        this.data.plasmapool,
        TickUtil.getStartTickIndex(this.data.tickUpperIndex, plasmapool.tickSpacing)
      ).publicKey,
      positionAuthority: positionWalletKey,
    });
    txBuilder.addInstruction(increaseIx);
    return txBuilder;
  }

  async decreaseLiquidity(
    liquidityInput: DecreaseLiquidityInput,
    sourceWallet?: Address,
    positionWallet?: Address,
    resolveATA?: boolean,
    ataPayer?: Address
  ) {
    const sourceWalletKey = sourceWallet
      ? AddressUtil.toPubKey(sourceWallet)
      : this.ctx.wallet.publicKey;
    const positionWalletKey = positionWallet
      ? AddressUtil.toPubKey(positionWallet)
      : this.ctx.wallet.publicKey;
    const ataPayerKey = ataPayer ? AddressUtil.toPubKey(ataPayer) : this.ctx.wallet.publicKey;
    const plasmapool = await this.fetcher.getPool(this.data.plasmapool, true);

    if (!plasmapool) {
      throw new Error("Unable to fetch plasmapool for this position.");
    }

    const txBuilder = new TransactionBuilder(this.ctx.provider);
    let tokenOwnerAccountA: PublicKey;
    let tokenOwnerAccountB: PublicKey;

    if (resolveATA) {
      const [ataA, ataB] = await resolveOrCreateATAs(
        this.ctx.connection,
        sourceWalletKey,
        [{ tokenMint: plasmapool.tokenMintA }, { tokenMint: plasmapool.tokenMintB }],
        () => this.fetcher.getAccountRentExempt(),
        ataPayerKey
      );
      const { address: ataAddrA, ...tokenOwnerAccountAIx } = ataA!;
      const { address: ataAddrB, ...tokenOwnerAccountBIx } = ataB!;
      tokenOwnerAccountA = ataAddrA;
      tokenOwnerAccountB = ataAddrB;
      txBuilder.addInstruction(tokenOwnerAccountAIx);
      txBuilder.addInstruction(tokenOwnerAccountBIx);
    } else {
      tokenOwnerAccountA = await deriveATA(sourceWalletKey, plasmapool.tokenMintA);
      tokenOwnerAccountB = await deriveATA(sourceWalletKey, plasmapool.tokenMintB);
    }

    const decreaseIx = decreaseLiquidityIx(this.ctx.program, {
      ...liquidityInput,
      plasmapool: this.data.plasmapool,
      position: this.address,
      positionTokenAccount: await deriveATA(positionWalletKey, this.data.positionMint),
      tokenOwnerAccountA,
      tokenOwnerAccountB,
      tokenVaultA: plasmapool.tokenVaultA,
      tokenVaultB: plasmapool.tokenVaultB,
      tickArrayLower: PDAUtil.getTickArray(
        this.ctx.program.programId,
        this.data.plasmapool,
        TickUtil.getStartTickIndex(this.data.tickLowerIndex, plasmapool.tickSpacing)
      ).publicKey,
      tickArrayUpper: PDAUtil.getTickArray(
        this.ctx.program.programId,
        this.data.plasmapool,
        TickUtil.getStartTickIndex(this.data.tickUpperIndex, plasmapool.tickSpacing)
      ).publicKey,
      positionAuthority: positionWalletKey,
    });
    txBuilder.addInstruction(decreaseIx);
    return txBuilder;
  }

  private async refresh() {
    const account = await this.fetcher.getPosition(this.address, true);
    if (!!account) {
      this.data = account;
    }
  }
}
