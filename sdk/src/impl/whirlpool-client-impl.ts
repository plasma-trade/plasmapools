import { AddressUtil } from "@plasmatrade-so/common-sdk";
import { Address } from "@project-serum/anchor";
import { PlasmaPoolContext } from "../context";
import { AccountFetcher } from "../network/public";
import { PlasmaPoolData, TokenInfo } from "../types/public";
import { PlasmaPoolClient, PlasmaPool, Position } from "../plasmapool-client";
import { PositionImpl } from "./position-impl";
import { PlasmaPoolImpl } from "./plasmapool-impl";

export class PlasmaPoolClientImpl implements PlasmaPoolClient {
  constructor(readonly ctx: PlasmaPoolContext, readonly fetcher: AccountFetcher) {}

  public getFetcher(): AccountFetcher {
    return this.fetcher;
  }

  public async getPool(poolAddress: Address, refresh = false): Promise<PlasmaPool> {
    const account = await this.fetcher.getPool(poolAddress, refresh);
    if (!account) {
      throw new Error(`Unable to fetch PlasmaPool at address at ${poolAddress}`);
    }
    const tokenInfos = await getTokenInfos(this.fetcher, account, false);
    return new PlasmaPoolImpl(
      this.ctx,
      this.fetcher,
      AddressUtil.toPubKey(poolAddress),
      tokenInfos[0],
      tokenInfos[1],
      account
    );
  }

  public async getPosition(positionAddress: Address, refresh = false): Promise<Position> {
    const account = await this.fetcher.getPosition(positionAddress, refresh);
    if (!account) {
      throw new Error(`Unable to fetch Position at address at ${positionAddress}`);
    }
    return new PositionImpl(this.ctx, this.fetcher, AddressUtil.toPubKey(positionAddress), account);
  }
}

async function getTokenInfos(
  fetcher: AccountFetcher,
  data: PlasmaPoolData,
  refresh: boolean
): Promise<TokenInfo[]> {
  const mintA = data.tokenMintA;
  const infoA = await fetcher.getMintInfo(mintA, refresh);
  if (!infoA) {
    throw new Error(`Unable to fetch MintInfo for mint - ${mintA}`);
  }
  const mintB = data.tokenMintB;
  const infoB = await fetcher.getMintInfo(mintB, refresh);
  if (!infoB) {
    throw new Error(`Unable to fetch MintInfo for mint - ${mintB}`);
  }
  return [
    { mint: mintA, ...infoA },
    { mint: mintB, ...infoB },
  ];
}
