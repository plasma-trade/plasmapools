# PlasmaPools

Whirpools is an open-source concentrated liquidity AMM contract on the Solana blockchain.
The PlasmaPools Typescript SDK (`@plasmatrade-so/plasmapools-sdk`) allows for easy interaction with a deployed PlasmaPools program.

The contract has been audited by Kudelski and Neodyme.

# PlasmaPool SDK

Use the SDK to interact with a deployed PlasmaPools program via Typescript.

## Installation

In your package, run:

```
yarn add `@plasmatrade-so/plasmapools-sdk`
yarn add "@project-serum/anchor"
yarn add "decimal.js"
```

## Usage

Read instructions on how to use the SDK on the [PlasmaTrade Developer Portal](https://plasmatrade-so.gitbook.io/plasmatrade-developer-portal/plasmatrade/welcome).

# License

[Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)
